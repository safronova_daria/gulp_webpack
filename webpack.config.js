'use strict'; //Включение строгого режима

const env = process.env.NODE_ENV; //Переменная, используемая для изменения mode в конфигурации Webpack
const { src, path, globby, glob, fs } = require('./helper.js');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const WebpackAssetsManifest = require('webpack-assets-manifest');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const styles = path.resolve(__dirname, 'app/styles/global.scss');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const SpriteLoaderPlugin = require('svg-sprite-loader/plugin');
const svg = glob.sync('./app/images/icons/*.svg');
// const globImporter = require('node-sass-glob-importer');


function generateHtmlPlugins(templateDir) {
  const templateFiles = fs.readdirSync(path.resolve(__dirname, templateDir));
  //Перебираем доступные файлы
  return templateFiles.map(item => {
    //Вытаскиваем название, расширение файла 
    const parts = item.split('.');
    const name = parts[0];
    const extension = parts[1];
    return new HtmlWebpackPlugin({ //Плагин, который создает HTML-файл на основе шаблона
      filename: `${name}.html`, //Файл, в который будет записан HTML
      template: path.resolve(__dirname, `${templateDir}/${name}.${extension}`), //Путь к шаблону
      chunks: [`${name}`, 'styles', 'headerTemplate', 'footerTemplate'] //Добавление определенных чанков к файлу
    })
  })
};


module.exports = async () => { //Настройки Webpack

  let entry = {};
  const paths = await globby(src('./app/js/*.js')); //Выбор путей
  const htmlPlugins = generateHtmlPlugins('app/pages/') //Генерация плагинов

  paths.forEach((src) => {
    entry[path.basename(src).replace(/\..*$/, "")] = src; //Изменение имени файла
  });

  return {
    entry: { //Точка входа 
      styles, //Стили SCSS
      svg, //svg иконки
      ...entry //Объект, хранящий название и путь файла
    },
    output: { //Точка выхода
      filename: '[name].bundle.js', //Название собранного бандла
      path: path.resolve(__dirname, './build') //Путь, куда будет помещен бандл
    },
    module: {
      rules: [{ //Массив, состоящий из загрузчиков
          test: /\.js$/, //Тип обрабатываемого файла - .js
          exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/, //Файлы, которые не нужно обрабатывать
          loader: 'babel-loader' //загрузчик, используемый для обработки файлов .js
        },
        {
          enforce: 'pre', //Тип загрузчика - предзагрузчик
          test: /\.(js|vue)$/,
          exclude: /node_modules/,
          loader: 'eslint-loader',
          options: { //Настройки загрузчика
            fix: true, //Включение ESLint autofix
            quiet: true, //Отображение ошибок, игнорирование предупреждений
            formatter: require('eslint-formatter-pretty') //Красивый вывод для ESLint
          }
        },
        {
          test: /\.vue$/,
          loader: 'vue-loader',
          options: {
            loaders: {
              scss: ['vue-style-loader', 'css-loader', 'postcss-loader'] //Какие загрузчики Webpack переопределят стандартные загрузчики, используемые для обработки .vue файлов
            }
          }
        },
        {
          test: /\.(css|scss)$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                options: {
                  url: false, //Отключение обработки функций url()
                }
              }
            },
            { loader: "css-loader",
              options: {
                url: false,
                sourceMap: true, //Включение генерации sourcemap
              }
            },
            {
              loader: 'postcss-loader',
              options: {
                sourceMap: true
              }
            },
            { loader: "sass-loader",
              options: {
                sourceMap: true,
                // importer: globImporter()
              }
            },
            { loader: 'sass-resources-loader',
              options: {
                resources: [ //То, что должно быть включено в файлы
                  path.resolve(__dirname, 'app/styles/helpers/variables.scss'),
                  path.resolve(__dirname, 'app/styles/helpers/mixins.scss')
                ]
              }
            }
          ]
        },
        {
          test: /\.(png|jpe?g|gif|woff|woff2?)$/,
          exclude: path.resolve(__dirname, "app/images/icons"),
          use: [
            {
              loader: "file-loader",
              options: {
                esModule: false, //Включение синтаксиса CommonJS 
                name: "[name].[ext]", //Генерация имени файла
              }
            }
          ]
        },
        {
          test: /\.svg$/,
          use: [
            {
              loader: 'svg-sprite-loader',
              options: {
                extract: true, //Переключает загрузчик в режим извлечения
                publicPath: 'images/', //Путь к спрайту
                spriteFilename: 'sprite.svg' //Имя извлеченного спрайта
              }
            },
            {
              loader: "svgo-loader",
              options: {
                plugins: [
                  { removeTitle: true }, //Удаление <title>
                  // {
                  //   removeAttrs: {
                  //     attrs: "(fill|stroke)"
                  //   }
                  // }
                ]
              }
            }
          ]
        }
      ]
    },
    devServer: { //Настройки сервера для разработки
      historyApiFallback: true, //Возвращение к index.html, если страницв не найдена
      noInfo: false, //Информация о Webpack, которая отображется при запуске, будет показываться
      overlay: true, //Показывает полноэкранный оверлей в браузере при ошибках или предупреждениях компилятора
      port: 9000 //Порт для сервера
    },
    optimization: { //Настройки оптимизации - разделения кода
      runtimeChunk: 'single', //Создает исполняемый файл, который будет использоваться всеми сгенерированными чанками
      splitChunks: { //Настройки для динамически имортируемых модулей
        chunks: 'all', //Все чанки будут оптимизированы
        maxInitialRequests: Infinity, //Максимум параллельных запросов в точке входа
        minSize: 0, //Минимальное время, в течение которого модуль должен быть поделен между чанками перед разделением
        cacheGroups: { //Позволяет наследовать и/или переопределять параметры из splitChunks и задавать дополнительные
          vendor: { //Vendor чанк
            test: /node_modules/, //Он содержит пакеты в node_modules
            chunks: "initial", //Он является основным чанком для точки входа
            name: "vendor", //Имя чанка
            enforce: true
          }
        },
      }
    },
    performance: { //Параметры для оповещения о том, что файлы превысили лимит
      hints: false //Отключение подсказок
    },
    resolve: {
      alias: { //Псевдонимы для модулей 
        vue$: env === 'development' ? 'vue/dist/vue.esm.js' : 'vue/dist/vue.min.js',
        '@': path.resolve(__dirname, "app/js"),
        '@components': path.resolve(__dirname, "app/js/vue/components"),
        '@helpers': path.resolve(__dirname, "app/js/helpers"),
        '@images': path.resolve(__dirname, "app/images"),
        '@vue': path.resolve(__dirname, "app/js/vue"),
      }
    },
    plugins: [ //Плагины
      new VueLoaderPlugin(), //Плагин для загрузки кода Vue
      new WebpackAssetsManifest({ //Плагин, который генерирует json файл, в котором находятся оригинальные названия файлов и названия с хэшем
        entrypoints: true, //Включает compilation.entrypoints
        transform: assets => assets.entrypoints //Обратный вызов для преобразования
      }),
      new MiniCssExtractPlugin({ //Плагин, который делит CSS на отдельные файлы
        filename: '[name].[hash:8].css', //Имя выходного файла
        allChunks: true
      }),
      new CopyWebpackPlugin({ //Плагин для копирования файлов 
        patterns: [ //Шаблоны
          {
            from: 'app/images', //Откуда копировать
            globOptions: {
              ignore: path.resolve(__dirname, "app/images/icons")
            },
            to: 'images' //Куда копировать
          },
          {
            from: 'app/fonts',
            to: 'fonts'
          }
        ],
      }),
      new SpriteLoaderPlugin({ plainSprite: true }) //Плагин для загрузки svg спрайтов
    ].concat(htmlPlugins), //Добавление плагинов, сгенерированных в функции выше
    devtool: env === 'development' ? '#eval-source-map' : ''
  }
};
