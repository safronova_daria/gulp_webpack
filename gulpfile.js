const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const rename = require('gulp-rename');
const sass = require('gulp-sass')(require('sass'));
const autoprefixer = require('gulp-autoprefixer');
const { parallel } = require('gulp');

function toSyncBrowser() {
    browserSync.init({
        server: { 
            baseDir: './' 
        },
        notify: false,
        port: 3000
    })
}

function changeCss(done) {
    gulp.src('./scss/**/*.scss')
    .pipe(sass({
        outputStyle: 'compressed'
    }))
    .pipe(autoprefixer({
        overrideBrowserslist: ['last 2 versions'],
        cascade: false
    }))
    .pipe(rename({
        suffix: '.min'
    }))
    .pipe(gulp.dest('./css/'))
    .pipe(browserSync.stream());
    done();
}

function watch() {
    gulp.watch("./scss/**/*", changeCss);
    gulp.watch("./**/*.html", browserReload);
    gulp.watch("./js/**/*.js", browserReload);
}

function browserReload(done) {
    browserSync.reload();
    done();
}

exports.toSyncBrowser = toSyncBrowser;
exports.changeCss = changeCss;
exports.default = parallel(toSyncBrowser, watch);